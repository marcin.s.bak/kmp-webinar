//
//  ContentView.swift
//  ios-app
//
//  Created by Marcin Bak on 25/08/2020.
//  Copyright © 2020 Marcin Bak. All rights reserved.
//

import SwiftUI
import frontend_shared

struct ContentView: View {
    var body: some View {
        Text(Platform.init().welcomeText)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
