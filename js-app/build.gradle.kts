plugins {
    id("org.jetbrains.kotlin.js")
}

dependencies {
    implementation(project(":shared-library"))
}

kotlin {
    js {
        browser()
    }
}