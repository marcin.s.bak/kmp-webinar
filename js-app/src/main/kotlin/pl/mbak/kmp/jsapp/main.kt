package pl.mbak.kmp.jsapp

import kotlinx.browser.document
import pl.mbak.kmp.shared.Platform

fun main() {
    document.write(Platform.welcomeText)
}